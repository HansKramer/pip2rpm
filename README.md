Summary
-------
pip might be convenient for developers, however, on an RPM based system 
I find it kind of ugly. There are other tools to convert pip to RPM. However,
none really worked for me. Instead of trying to compile the code, I am going to
rely on the pip system to do this worked for me, and then I just wrap it up
in an RPM. To not mess up my own system, I am doing this inside a docker container.
After a successful build the container gets dropped, and there are no nasty dependencies.
Furthermore, I can do RPM build for other versions of any RPM supported OS.
Currently I have only tried it on CentOS and Fedora 20

Requirements
------------
1. Docker 
2. python-docker-py

Usage
-----
 usage: pip2rpm-build [-h] -p PACKAGE [-r PACKAGE_VERSION] [-V PYTHON_VERSION]  
                     -j OS -w OS_VERSION [-d DESTINATION] [-D] [-x RPM] [-v]  
                     [-q] [-k]

optional arguments:  
  -h, --help            show this help message and exit  
  -p PACKAGE, --package PACKAGE  
  -r PACKAGE_VERSION, --package-version PACKAGE_VERSION  
  -V PYTHON_VERSION, --python-version PYTHON_VERSION  
  -j OS, --os OS  
  -w OS_VERSION, --os-version OS_VERSION  
  -d DESTINATION, --destination DESTINATION  
  -D, --build-deps  
  -x RPM, --rpm RPM  
  -v, --verbose  
  -q, --quiet  
  -k, --keep-container  

Examples

python pip2rpm-build --package=PyGreSQL --python-version=2 --rpm=python-devel --rpm=postgresql-devel --rpm=gcc --os=centos --os-version=7.5.1804


