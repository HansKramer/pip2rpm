#
#  \Author Hans Kramer
#
#  \Date   Aug 2018


from   subprocess import Popen, PIPE
import tarfile
import os
import argparse
import imp

parser  = argparse.ArgumentParser()
parser.add_argument("-p", "--package", required=True)
parser.add_argument("-v", "--version", required=False)
parser.add_argument("-V", "--python-version", required=False, default=2, type=int)
args    = parser.parse_args()
package = args.package
version = args.version

python = "python" if args.python_version == 2 else "python%d" % args.python_version

p = Popen([python, "-m", "pip", "show", "-f", package], stdout=PIPE)

spec = {}
for l in p.stdout:
    v = l.decode('utf-8').split(":")
    if v[0] == "Files":
        spec["Files"] = []
        for file in p.stdout:
            spec["Files"].append(file.decode('utf-8').strip())
        break
    spec[v[0]] = v[1].strip()


target_dir = "SOURCES"
if not os.path.exists(target_dir):
    os.mkdir(target_dir)
tar_name   = "%s-%s.tar" % (spec["Name"], spec["Version"])
basedir    = python + "-" + spec["Name"] + "-" + spec["Version"]

tar        = tarfile.open(os.path.join(target_dir, tar_name), "w")


def tar_add(tar, spec, basedir, file):
    p = os.path.abspath(os.path.join(spec['Location'], file))
    arcname = os.path.join(basedir, p[1:])
    tar.add(p, arcname=arcname)
    return p + "\n"


files = ""
for file in spec['Files']:
    # fix broken pip show -f
    if args.python_version == 3 and os.path.splitext(file)[1] == ".pyc" and not file.startswith("__pycache__/"):
        name = os.path.splitext(file)
        files += tar_add(tar, spec, basedir, "__pycache__/" + name[0] + "." + imp.get_tag() + name[1])
        #print("__pycache__/" + name[0] + "." + imp.get_tag() + name[1])
    else:
        files += tar_add(tar, spec, basedir, file)

tar.close()

fp_in = open("pip2rpm.template", "r")
spec_file = "".join(fp_in.readlines())


requires = [python]
if len(spec["Requires"]) > 0:
    requires += [python + "-" + x.strip() for x in spec["Requires"].split(",")]

spec_file = spec_file.format(name     = spec["Name"],  
                             version  = spec["Version"], 
                             python   = "python" if args.python_version == 2 else python,
                             summary  = spec["Summary"], 
                             license  = spec["License"], 
                             requires = ",".join(requires), 
                             source   = tar_name, 
                             description = spec["Summary"], 
                             files    = files)

fp_out = open("python-%s.spec" % package, "w")
fp_out.write(spec_file)
fp_out.close()
