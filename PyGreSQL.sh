#! /bin/bash

sudo docker rm -f pip 2> /dev/null 1> /dev/null

PYTHON2_ARGS="--python-version=2 --rpm=python-devel"
PYTHON3_ARGS="--python-version=3 --rpm=python3-devel"
POSTGRES="--package=PyGreSQL --rpm=postgresql-devel --rpm=gcc"


for version in {20..29} ; do
    sudo ./pip2rpm-build $POSTGRES $PYTHON2_ARGS --os=fedora --os-version=$version -d output
    sudo ./pip2rpm-build $POSTGRES $PYTHON3_ARGS --os=fedora --os-version=$version -d output
done

for version in 7.0.1406 7.1.1503 7.2.1511 7.3.1611 7.4.1708 7.5.1804; do
    sudo ./pip2rpm-build $POSTGRES $PYTHON2_ARGS --os=centos --os-version=$version -d output
done

for package_version in 3.8.1 4.0 4.1 4.1.1 4.2 4.2.1 4.2.2 5.0 5.0.1 5.0.2 5.0.3 5.0.4 5.0.6; do
    sudo ./pip2rpm-build $POSTGRES $PYTHON2_ARGS --os=fedora --os-version=20 --package-version=$package_version  -d output
    sudo ./pip2rpm-build $POSTGRES $PYTHON2_ARGS --os=centos --os-version=7.4.1708 --package-version=$package_version  -d output
done

for package_version in 5.0 5.0.1 5.0.2 5.0.3 5.0.4 5.0.6; do
    sudo ./pip2rpm-build $POSTGRES $PYTHON3_ARGS --os=fedora --os-version=20 --package-version=$package_version  -d output
done
